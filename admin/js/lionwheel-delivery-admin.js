/**
 * Lionwheel delivery plugin
 * Order page @ admin screen
 * Ajax requests
 */
(function ($) {
    'use strict';
    $(document).ready(function ($) {


        $('.ship_status').click(function (e) {
            e.preventDefault();
            var lionwheel_ship_id = $(this).data('status');

            $.ajax({
                url: lionwheel_delivery.ajax_url,
                type: 'POST',
                data: {
                    action: 'lionwheel_get_order_details',
                    lionwheel_order_id: lionwheel_ship_id,
                    lionwheel_get_wpnonce: lionwheel_delivery.lionwheel_ajax_get_nonce
                },
                success: function (response) {
                    var lionwheel_response = JSON.parse(response);

                    console.log(lionwheel_response);

                    var lionwheel_status = lionwheel_response.Records.Record.DeliveryStatus;

                    var delivery_status='';
                   if (lionwheel_status != 1 && lionwheel_status != 2) {
                        $(".lionwheel_receiver_name").html(lionwheel_response.Records.Record.Receiver);
                        $(".lionwheel_shipped_on").html(lionwheel_response.Records.Record.ExeTime);
                    }


                    switch (lionwheel_status) {
                        case "1":
                            delivery_status = lionwheel_delivery.lionwheel_status_1;
                            break;
                        case "2":
                            delivery_status = lionwheel_delivery.lionwheel_status_2;
                            break;
                        case "3":
                            delivery_status = lionwheel_delivery.lionwheel_status_3;
                            break;
                        case "4":
                            delivery_status = lionwheel_delivery.lionwheel_status_4;
                            break;
                        case "5":
                            delivery_status = lionwheel_delivery.lionwheel_status_5;
                            break;
                        case "7":
                            delivery_status = lionwheel_delivery.lionwheel_status_7;
                            break;
                        case "8":
                            delivery_status = lionwheel_delivery.lionwheel_status_8;
                            break;
                        case "9":
                            delivery_status = lionwheel_delivery.lionwheel_status_9;
                            break;
                        case "12":
                            delivery_status = lionwheel_delivery.lionwheel_status_12;
                            break;
                    }




                    $(".lionwheel_delivery_status").html(delivery_status);

                    //console.log(lionwheel_response.Records.Record.Receiver);
                    $(".lionwheel_receiver_name").html(lionwheel_response.Records.Record.Receiver);
                    $(".lionwheel_shipped_on").html(lionwheel_response.Records.Record.ExeTime);

                }
            })

        });

        /**
         * If the ship is already open - get ship details
         */
        // if(document.getElementById("lionwheel_ship_exists") != null) {
        //     $(".lionwheel-powered-by").hide();
        //     $("#lionwheel_ship_exists").append('<img class="lionwheel_loader" src="'+lionwheel_delivery.lionwheel_ajax_loader+'">');
        //     var lionwheel_ship_id = $(".lionwheel_delivery_id").text();
        //     $.ajax({
        //         url: lionwheel_delivery.ajax_url,
        //         type: 'POST',
        //         data: {
        //             action : 'lionwheel_get_order_details',
        //             lionwheel_order_id: lionwheel_ship_id,
        //             lionwheel_get_wpnonce: lionwheel_delivery.lionwheel_ajax_get_nonce
        // },
        //     success: function( response ) {
        //         var delivery_status = "";
        //         $(".lionwheel_loader").hide();
        //         $(".lionwheel-powered-by").show();
        //         if (response == 'Communication error') {
        //             $("#lionwheel_ship_exists").append('<p class="lionwheel_error_message">'+lionwheel_delivery.lionwheel_err_message+'</p>');
        //         } else {
        //             var lionwheel_response = JSON.parse(response);
        //             if (lionwheel_response.StatusCode == "-999") {
        //                 $(".lionwheel_exist_details").replaceWith("<h4>"+ lionwheel_delivery.lionwheel_err_message_code + "</h4>");
        //             } else if (Object.keys(lionwheel_response.Records).length == 0) {
        //                 $("#lionwheel_ship_exists").append('<p class="lionwheel_error_message">'+lionwheel_delivery.lionwheel_err_message_code+'</p>');
        //             }else if (lionwheel_response.StatusCode == "-100") {
        //                 $(".lionwheel_exist_details").replaceWith("<h4>"+ lionwheel_delivery.lionwheel_err_message_code + "</h4>");
        //             } else {
        //                 var lionwheel_status = lionwheel_response.Records.Record.DeliveryStatus;
        //                 switch (lionwheel_status) {
        //                     case "1":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_1;
        //                         break;
        //                     case "2":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_2;
        //                         break;
        //                     case "3":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_3;
        //                         break;
        //                     case "4":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_4;
        //                         break;
        //                     case "5":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_5;
        //                         break;
        //                     case "7":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_7;
        //                         break;
        //                     case "8":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_8;
        //                         break;
        //                     case "9":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_9;
        //                         break;
        //                     case "12":
        //                         delivery_status = lionwheel_delivery.lionwheel_status_12;
        //                         break;
        //                 }
        //                 $(".lionwheel_exist_details").show();
        //
        //                 if (lionwheel_status == 8) {
        //                     $(".lionwheel_ship_open").hide();
        //                     $(".lionwheel-button-container").append('<button class="lionwheel-button lionwheel-reopen-ship">'+lionwheel_delivery.lionwheel_reopen_ship+'</button>');
        //                 } else if (lionwheel_status != 1 && lionwheel_status != 2){
        //                     $(".lionwheel_receiver_name").html(lionwheel_response.Records.Record.Receiver);
        //                     $(".lionwheel_shipped_on").html(lionwheel_response.Records.Record.ExeTime);
        //                 } else {
        //                     $(".lionwheel_ship_open").hide();
        //                 }
        //                 $(".lionwheel_delivery_status").html(delivery_status);
        //                 if (lionwheel_status == 1){
        //                     $(".lionwheel-button-container").append('<button class="lionwheel-button lionwheel-cancel-ship">'+lionwheel_delivery.lionwheel_cancel_ship+'</button>');
        //                 }
        //             }
        //         }
        //     },
        //     error: function () {
        //         $(".lionwheel_loader").hide();
        //         $(".lionwheel-powered-by").show();
        //         $("#lionwheel_ship_exists").append('<p class="lionwheel_error_message">'+lionwheel_delivery.lionwheel_err_message+'</p>');
        //         }
        //     });
        //
        // }
        /**
         * On click open new Lionwheel delivery
         */
        $(document).on("click", ".lionwheel-open-button", function (event) {
            event.preventDefault();
            $(".lionwheel-open-button").hide();
            $(".lionwheel-powered-by").hide();
            $("#lionwheel_open_ship").append('<img class="lionwheel_loader" src="' + lionwheel_delivery.lionwheel_ajax_loader + '">');
            var lionwheel_order_id = $("button.lionwheel-open-button").attr("data-order");
            //var lionwheel_urgent = "1";
            //if ($('#lionwheel_urgent').attr('checked')) {
               // lionwheel_urgent = "2"
            //}
            var lionwheel_return = false;

            if ($('#lionwheel_return').is(':checked')) {
                lionwheel_return = true;
            }
            var lionwheel_collect = $('#collect').val();
            console.log("lionwheel_collect: "+lionwheel_collect);
            if (isNaN(lionwheel_collect)) {
                lionwheel_collect = 'NO';
            } else {
                lionwheel_collect = lionwheel_collect * 100;
            }
            //var lionwheel_motor = $('#lionwheel_motor').val();
            var lionwheel_packages = $('#lionwheel_packages').val();
            var lionwheel_exaction_date = $('#lionwheel_exaction_date').val();

            //var lionwheel_delivey_type = $('input[name=lionwheel_delivey_type]:checked').val();


            $.ajax({
                url: lionwheel_delivery.ajax_url,
                type: 'POST',
                data: {
                    action: 'lionwheel_open_new_order',
                    lionwheel_order_id: lionwheel_order_id,
                    //lionwheel_urgent: lionwheel_urgent,
                    lionwheel_return: lionwheel_return,
                    lionwheel_collect: lionwheel_collect,
                    //lionwheel_motor: lionwheel_motor,
                    lionwheel_packages: lionwheel_packages,
                    lionwheel_exaction_date: lionwheel_exaction_date,
                    //lionwheel_delivey_type: lionwheel_delivey_type,
                    lionwheel_wpnonce: lionwheel_delivery.lionwheel_ajax_nonce
                },
                success: function (response) {
                    $(".lionwheel_loader").hide();
                    $(".lionwheel-powered-by").show();
                    $(".lionwheel-open-button").hide();
                    if (response == '-100') {
                        $("#lionwheel_open_ship").append('<p class="lionwheel_error_message">' + lionwheel_delivery.lionwheel_err_message_open_code + '</p>');
                    } else if (response == '-999') {
                        $("#lionwheel_open_ship").append('<p class="lionwheel_error_message">' + lionwheel_delivery.lionwheel_err_message + '</p>');
                    }
                    if ( Number(response ) < 0 ) {
                        alert( response );
                    } else {
                        location.reload();
                    }
                },
                error: function () {

                    $(".lionwheel_loader").hide();
                    $(".lionwheel-powered-by").show();
                    $("#lionwheel_open_ship").append('<p>' + lionwheel_delivery.lionwheel_err_message + '</p>');
                    location.reload();
                }
            })
        });

        /**
         * On click change Lionwheel delivery status
         */
        $(document).on("click", ".lionwheel-cancel-ship", function (event) {
            event.preventDefault();
            $("#lionwheel_ship_exists").hide();
            $(".lionwheel-powered-by").hide();
            var container = $(this).closest('.lionwheel-button-container');
            container.append('<img class="lionwheel_loader" src="' + lionwheel_delivery.lionwheel_ajax_loader + '">');
            // var lionwheel_ship_id = $(".lionwheel_delivery_id").text();
            var lionwheel_ship_id = $(this).data('shipping-id');
            var order_id = $(this).data('order-id');


            $.ajax({
                url: lionwheel_delivery.ajax_url,
                type: 'POST',
                data: {
                    action: 'lionwheel_change_order_status',
                    lionwheel_ship_id: lionwheel_ship_id,
                    order_id: order_id,
                    lionwheel_change_wpnonce: lionwheel_delivery.lionwheel_ajax_change_nonce
                },
                success: function (response) {
                    console.log(response);
                    $(".lionwheel_loader").hide();
                    if (response == '-100') {
                        $(".lionwheel-wrapper").append('<p class="lionwheel_error_message">' + lionwheel_delivery.lionwheel_err_message_open_code + '</p>');
                    } else if (response == '-999') {
                        $(".lionwheel-wrapper").append('<p class="lionwheel_error_message">' + lionwheel_delivery.lionwheel_err_message + '</p>');
                    } else if (response == '1') {
                        location.reload();
                        //  $(".lionwheel-wrapper").append('<p class="lionwheel_error_message">'+lionwheel_delivery.lionwheel_cancel_ship_ok+'</p>');
                    } else {
                        $(".lionwheel-wrapper").append('<p class="lionwheel_error_message">' + lionwheel_delivery.lionwheel_err_message + '</p>');
                    }
                },
                error: function () {
                    $(".lionwheel_loader").hide();
                    container.append('<p>' + lionwheel_delivery.lionwheel_err_message + '</p>');
                }
            })
        });
        /**
         * On click change Lionwheel delivery status
         */
        $(document).on("click", ".lionwheel-reopen-ship", function (event) {
            event.preventDefault();
            $("#lionwheel_ship_exists").hide();
            $(".lionwheel-powered-by").hide();
            $(".lionwheel-wrapper").append('<img class="lionwheel_loader" src="' + lionwheel_delivery.lionwheel_ajax_loader + '">');
            var lionwheel_woo_order_id = $("#lionwheel_ship_exists").attr("data-order");
            $.ajax({
                url: lionwheel_delivery.ajax_url,
                type: 'POST',
                data: {
                    action: 'lionwheel_reopen_ship',
                    lionwheel_woo_order_id: lionwheel_woo_order_id,
                    lionwheel_reopen_wpnonce: lionwheel_delivery.lionwheel_ajax_reopen_nonce
                },
                success: function (response) {
                    location.reload();
                },
                error: function () {
                    $(".lionwheel_loader").hide();
                    $(".lionwheel-powered-by").show();
                    $(".lionwheel-wrapper").append('<p>' + lionwheel_delivery.lionwheel_err_message + '</p>');
                }
            })
        });
        /**
         * Put NIS mark next to the collect box
         */
        $("#lionwheel_collect").change(function () {
            if ($(".lionwheel_nis").length == 0) {
                $("#lionwheel_collect").after("<span class='lionwheel_nis'>&#8362</span>");
            }

        });
    })
})(jQuery);


