<?php

/**
 * @wordpress-plugin
 * Plugin Name:       LionWheel Delivery
 * Plugin URI:        https://www.lionwheel.com
 * Description:       LionWheel integration with WooCommerce
 * Version:           1.0.0
 * Author URI:        https://www.lionwheel.com
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-lionwheel-delivery.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
define('PLUGIN_NAME', 'lionwheel-delivery');
define('PLUGIN_VERSION', '1.0.0');
//Don't forget to put / in the end of the URL!
define('LIONWHEEL_DEFAULT_URL', 'https://members.lionwheel.com/api/v1/');
define('LIONWHEEL_CREATE', 'create');
define('LIONWHEEL_GET_BY_ID', 'ListDeliveryDetails');
define('LIONWHEEL_SAVE_NEW', '');
//define('LIONWHEEL_CHANGE_STATUS', '');

register_activation_hook( __FILE__, 'activate_lionwheel_delivery' );

/**
 * store the plugin install date
 */
function activate_lionwheel_delivery() {
	$lionwheel_install_date_db = get_option( 'lionwheel_install_date' );
	if(empty( $lionwheel_install_date_db ) ) {
		add_option('lionwheel_install_date', date("d-m-Y") );
	} else {
		update_option('lionwheel_install_date', date("d-m-Y"));
	}
}

/**
 * Run plugin
 */
function run_lionwheel_delivery() {
	$plugin = new Lionwheel_Delivery();
	$plugin->run();
}

/**
 * Check if WooCommerce is installed
 */
if (in_array('woocommerce/woocommerce.php', get_option('active_plugins'))) {
	run_lionwheel_delivery();
} else {
	add_action( 'admin_notices', 'lionwheel_woo_error_notice' );
}

/**
 * Load Text domain
 */
add_action( 'init',function () {
	load_plugin_textdomain( 'woo-lionwheel-delivery', null, dirname( plugin_basename( __FILE__ ) ) . '/languages');
});

/**
 * Add link to plugin list
 */
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'lionwheel_action_links' );

function lionwheel_action_links( $links ) {
	$links[] = '<a href="'. esc_url( get_admin_url(null, 'options-general.php?page=lionwheel-delivery-options') ) .'">'.__( 'Settings', 'woo-lionwheel-delivery' ).'</a>';
	return $links;
}

/**
 * Error message if WooCommerce is not installed
 */
function lionwheel_woo_error_notice() {
	?>
	<div class="error notice">
		<p><?php _e( 'WooCommerce is not active. Please activate plugin before using Lionwheel Delivery plugin.', 'woo-lionwheel-delivery' ); ?></p>
	</div>
	<?php
}


/**
 * @param $bulk_array
 * @return mixed
 * Add new bulk edit to shop order
 */
function lionwheel_order_bulk_actions($bulk_array)
{

	$bulk_array['lionwheel_bulk'] = __('Create orders on lionwheel delivery.', 'woo-lionwheel-delivery');

	return $bulk_array;

}

add_filter('bulk_actions-edit-shop_order', 'lionwheel_order_bulk_actions');

/**
 * @param $redirect
 * @param $doaction
 * @param $object_ids
 * @return string
 * Add logic to create bulk edit
 */

function lionwheel_bulk_action_handler($redirect, $doaction, $object_ids)
{
	if ($doaction !== 'lionwheel_bulk') {
		return $redirect;
	}

	$lionwheel_service = new Lionwheel_service();

	$errors = array();
	foreach ( $object_ids as $order_id ) {

		$order  = wc_get_order( $order_id );
        $woo_order_key = $order->get_order_key();

		if ( ! $order ) {
			continue;
		}

		$data = get_post_meta( $order_id, '_lionwheel_ship_data');
		if ( ! empty( $data[0] ) && strpos( $data[0]['delivery_number'], '-' ) === false ) {
			$errors[$order_id] = sprintf( 'Error: Order id (%d) data exist.', $order_id );
			continue;
		}

		$shipping_details           = $order->get_address( 'shipping' );
		$billing_details            = $order->get_address( 'billing' );
		$customer_note              = $order->get_customer_note();
		$ship_data                  = array();
		$ship_data['street']        = $shipping_details['address_1'] . $shipping_details['address_2'];
		$ship_data['number']        = "";
		$ship_data['city']          = $shipping_details['city'];
		// $ship_data['company']       = $shipping_details['company'];
		$ship_data['note']          = $customer_note;
		// $ship_data['urgent']        = '1';
		// $ship_data['type']          = '1';
		// $ship_data['motor']         = '1';
		$ship_data['packages']      = '1';
		$ship_data['return']        = true;
		$ship_data['woo_key_id']    = $woo_order_key;
		$ship_data['woo_id']        = $order_id;
		$ship_data['extra_note']    = "";
		$ship_data['contact_name']  = $shipping_details['first_name'] . ' ' . $shipping_details['last_name'];
		$ship_data['contact_phone'] = $billing_details['phone'];
		$ship_data['contact_mail']  = $billing_details['email'];
		$ship_data['exaction_date'] =  date('Y-m-d');
		// $ship_data['collect']       = '';
		$ship_data['delivery_time'] = date( "d-m-Y g-i-s" );


		$response = $lionwheel_service->create_ship( $ship_data );

		switch ( $response[0] ) {
			case "-100":
				$status = 'Shipping create status: -100';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-999":
				$status = 'Shipping create status: -999';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-201":
				$status = 'Error - please check WooCommerce shipping Details.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-203":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-204":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-205":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-206":
				$status = 'Error - please check WooCommerce shipping street name.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-207":
				$status = 'Error - please check WooCommerce shipping house number.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-208":
				$status = 'Error - please check WooCommerce shipping city name.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-209":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-210":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-219":
				$status = 'Error - please check Lionwheel settings -> customer code.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-221":
				$status = 'Error - please check WooCommerce shipping note.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-222":
				$status = 'Error - please check shipping packages number.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-223":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-224":
				$status = 'Error - please check Lionwheel settings -> collect from address.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-225":
				$status = 'Error - please check WooCommerce shipping costumer name.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-226":
				$status = 'Error - please check WooCommerce shipping costumer phone number.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-227":
				$status = 'Error - please check WooCommerce shipping costumer email.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-228":
				$status = 'Error - please check shipping delivery date.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			case "-229":
				$status = 'Error - please check shipping collect amount.';
				$order->add_order_note( $status  );
				$errors[$order_id] = $status;
			default:
				$ship_data['delivery_number'] = $response[0];
				update_post_meta( $order_id, '_lionwheel_ship_data', $ship_data );

				$ship_type = '';
				if ( $_REQUEST['lionwheel_delivey_type'] == '1' ) {
					if ( $_REQUEST['lionwheel_return'] == '2' ) {
						$ship_type = __( 'Double delivery', 'woo-lionwheel-delivery' );
					} else {
						$ship_type = __( 'regular delivery', 'woo-lionwheel-delivery' );
					}
				} elseif ( $_REQUEST['lionwheel_delivey_type'] == '2' ) {
					$ship_type = __( 'Collecting delivery', 'woo-lionwheel-delivery' );
				}

				$order->add_order_note( sprintf( __( 'Shipping successfully created, response: %s', 'woo-lionwheel-delivery' ), $response[0] ) );
		}
	}

	return add_query_arg( 'lionwheel_bulk_done', $errors, $redirect);
}
add_filter('handle_bulk_actions-edit-shop_order', 'lionwheel_bulk_action_handler', 10, 3);

add_action( 'admin_notices', function () {
	if ( isset( $_GET['lionwheel_bulk_done'] ) && ! empty( $_GET['lionwheel_bulk_done'] ) ) {
		$class = 'notice notice-error';

		foreach ( $_GET['lionwheel_bulk_done'] as $order_id => $error ) {
			$message = sprintf( 'There is an error with order id (%d): %s', $order_id, $error );

			printf( '<div class="%1$s"><p>%2$s</p></div><br>', esc_attr( $class ), esc_html( $message ) );
		}
	}
});

/**
 * Return notice after bulf edit
 */
function lionwheel_bulk_action_notices()
{


	$li = '';
	if (isset($_REQUEST['lionwheel_bulk_done']['shipNumber'])) {
		foreach ($_REQUEST['lionwheel_bulk_done']['shipNumber'] as $shipping) {
			$li .= '<li>משלוח: ' . $shipping . '</li>';
		}

		// first of all we have to make a message,
		// of course it could be just "Posts updated." like this:
		if (!empty($_REQUEST['lionwheel_bulk_done'])) {
			$total = $_REQUEST['lionwheel_bulk_done']['total'];
			echo intval($_REQUEST['lionwheel_bulk_done']);
			echo '<div id="message" class="updated notice is-dismissible">
			<p>' . $total . 'נוצר </p>
			<ul>' . $li . '</ul>
		</div>';
		}
	}

}

add_action('admin_notices', 'lionwheel_bulk_action_notices');





