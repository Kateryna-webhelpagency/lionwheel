<?php
/**
 * Create a sub menu in WordPress settings menu
 */

if ( ! defined( 'ABSPATH' ) ) exit;

function register_lionwheel_settings_submenu() {

    add_submenu_page(
        'options-general.php',
        __( 'LionWheel Settings', 'woo-lionwheel-delivery' ),
        __( 'LionWheel Settings' , 'woo-lionwheel-delivery' ),
        'manage_options',
        'lionwheel-settings-page',
        'lionwheel_settings_page_options'
        );
}

function lionwheel_settings_page_options() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.', 'woo-lionwheel-delivery' ) );
    }
    if ( $_POST) {
        if ( empty($_POST) || ! wp_verify_nonce( $_POST['lionwheel_settings_nonce'], 'submit_lionwheel_settings') ){
            print 'Failed security check';
            exit;
        }
        if(empty($_POST["lionwheel_collect_token"])){
            ?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( 'The token field is empty. Please, enter your token', 'woo-lionwheel-delivery' ); ?></p>
            </div>
            <?php
        } else {

        foreach ( $_POST as $key => $value ) {
           if ( get_option( $key ) != $value ) {
                update_option( $key, $value );

            } else {
                add_option( $key, $value, '', 'no' );
            }

        }
        ?>
            <div class="notice notice-success is-dismissible">
                <p><?php _e( 'Your settings have been saved.', 'woo-lionwheel-delivery' ); ?></p>
            </div>
    <?php  }
    } ?>

    <style>
        label, input {
            display: inline-block;
        }
        label {
            width: 15%;
            min-width: 150px;
            padding: 0 10px;
        }
        input:not([type=submit]) {
            min-width: 40%;
            max-width: 470px;
        }
    </style>
    <div class="wrap">
        <h1><?php _e( 'LionWheel Delivery Settings', 'woo-lionwheel-delivery' ) ?></h1>
<?php

?>
        <form class="lionwheel-settings-form" method="POST">
            <?php wp_nonce_field('submit_lionwheel_settings','lionwheel_settings_nonce'); ?>
            <h2><?php _e( 'Token (provided by LionWheel):', 'woo-lionwheel-delivery' ) ?></h2>
            <p><label for="lionwheel_token"><?php _e( 'Input your token: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_token" name="lionwheel_collect_token" value="<?php if(get_option('lionwheel_collect_token')) { echo get_option('lionwheel_collect_token');}?>">
            </p>

            <h2 style="margin-top: 30px;"><?php _e( 'From:', 'woo-lionwheel-delivery' ) ?></h2>

            <p><label for="lionwheel_source_name"><?php _e( 'Source Name: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_name" name="lionwheel_collect_company_name" value="<?php if(get_option('lionwheel_collect_company_name')) { echo get_option('lionwheel_collect_company_name');}?>">
            </p>
            <p><label for="lionwheel_source_city"><?php _e( 'Source City: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_city" name="lionwheel_collect_city_name" value="<?php if(get_option('lionwheel_collect_city_name')) { echo get_option('lionwheel_collect_city_name');}?>">
            </p>
            <p><label for="lionwheel_source_street"><?php _e( 'Source Street: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_street" name="lionwheel_collect_street_name" value="<?php if(get_option('lionwheel_collect_street_name')) { echo get_option('lionwheel_collect_street_name');}?>">
            </p>
            <p><label for="lionwheel_source_number"><?php _e( 'Source number: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_number" name="lionwheel_collect_street_number" value="<?php if(get_option('lionwheel_collect_street_number')) { echo get_option('lionwheel_collect_street_number');}?>">
            </p>
            <p><label for="lionwheel_source_floor"><?php _e( 'Source floor: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="number" id="lionwheel_source_floor" name="lionwheel_collect_source_floor" value="<?php if(get_option('lionwheel_collect_source_floor')) { echo get_option('lionwheel_collect_source_floor');}?>">
            </p>
            <p><label for="lionwheel_source_apartment"><?php _e( 'Source Apartment: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_apartment" name="lionwheel_collect_source_apartment" value="<?php if(get_option('lionwheel_collect_source_apartment')) { echo get_option('lionwheel_collect_source_apartment');}?>">
            </p>
            <p><label for="lionwheel_source_note"><?php _e( 'Source Note: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_note" name="lionwheel_collect_source_note" value="<?php if(get_option('lionwheel_collect_source_note')) { echo get_option('lionwheel_collect_source_note');}?>">
            </p>
            <p><label for="lionwheel_source_contact_name"><?php _e( 'Source Contact Name: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="text" id="lionwheel_source_contact_name" name="lionwheel_username" value="<?php if(get_option('lionwheel_username')) { echo get_option('lionwheel_username');}?>">
            </p>
            <p><label for="lionwheel_source_phone"><?php _e( 'Source Phone number: ', 'woo-lionwheel-delivery' ) ?></label>
                <input type="tel" id="lionwheel_source_phone" name="lionwheel_collect_source_phone" value="<?php if(get_option('lionwheel_collect_source_phone')) { echo get_option('lionwheel_collect_source_phone');}?>">
            </p>
            <p><label for="lionwheel_source_email"><?php _e( 'Source Email: ', 'woo-ы-delivery' ) ?></label>
                <input type="email" id="lionwheel_source_email" name="lionwheel_collect_source_email" value="<?php if(get_option('lionwheel_collect_source_email')) { echo get_option('lionwheel_collect_source_email');}?>">
            </p>

            <?php submit_button(__( 'Submit settings', 'woo-lionwheel-delivery'  ), 'primary','submit-settings', false, array( 'id' => "submit-settings" ));?>

        </form>
    </div>
    <?php

}
