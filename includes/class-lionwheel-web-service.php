<?php

    /**
     * Class Lionwheel_service
     * This is used to communicate with LionWheel API
     */
    class Lionwheel_service
    {

        private $lionwheel_login = array();

        public function __construct()
        {
            $this->lionwheel_login['url'] = get_option('lionwheel_service_url', LIONWHEEL_DEFAULT_URL);
            $this->lionwheel_login['username'] = get_option('lionwheel_username');
            $this->lionwheel_login['password'] = get_option('lionwheel_password');
            $this->lionwheel_login['code'] = get_option('lionwheel_customer_code');
            $this->lionwheel_login['collect_street'] = get_option('lionwheel_collect_street_name');
            $this->lionwheel_login['collect_street_number'] = get_option('lionwheel_collect_street_number');
            $this->lionwheel_login['collect_city'] = get_option('lionwheel_collect_city_name');
            $this->lionwheel_login['collect_company'] = get_option('lionwheel_collect_company_name');

            $this->lionwheel_login['token'] = get_option('lionwheel_collect_token');
            $this->lionwheel_login['collect_floor'] = get_option('lionwheel_collect_source_floor');
            $this->lionwheel_login['collect_apartment'] = get_option('lionwheel_collect_source_apartment');
            $this->lionwheel_login['collect_note'] = get_option('lionwheel_collect_source_note');
            $this->lionwheel_login['collect_phone'] = get_option('lionwheel_collect_source_phone');
            $this->lionwheel_login['collect_email'] = get_option('lionwheel_collect_source_email');

        }

        /**
         * Open CURL with Lionwheel API
         * @param $data
         * @param $lionwheel_func
         *
         * @return mixed
         * @throws Exception
         */
        private function lionwheel_connection($data, $lionwheel_func)
        {
            $lionwheel_func = "tasks/create?key=" . $this->lionwheel_login['token'];

            $url = $this->lionwheel_login['url'] . $lionwheel_func;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($curl);

            if ($response === false) {
                echo "throw new Exception('Communication error') ";
                throw new Exception('Communication error');
            }
            curl_close($curl);


            return $response;

        }

        /**
         * Convert response from XML to JSON
         *
         * @param $lionwheel_response
         *
         * @return array
         */
        private function lionwheel_response_json($lionwheel_response)
        {
            $lionwheel_array = json_decode($lionwheel_response, 'ARRAY_A');

            return (array)$lionwheel_array;
        }

        /**
         * Get Lionwheel Ship Status
         * @param $lionwheel_id
         *
         * @return array
         */
        public function get_ship_status($lionwheel_id)
        {
            $data = array();
            $data['customerId'] = $this->lionwheel_login['code'];
            $data['deliveryNumbers'] = $lionwheel_id;
            try {
                $response = $this->lionwheel_connection($data, LIONWHEEL_GET_BY_ID);
                return $this->lionwheel_response_json($response);
            } catch (Exception $e) {
                print_r($e->getMessage());
                die();
            }
        }

        /**
         * Open Lionwheel ship
         * @param $ship_data
         *
         * @return array
         */
        public function create_ship($ship_data)
        {
            $ship_data['lionwheel_static'] = '0';
            $ship_data['lionwheel_empty'] = '';
//            if ($ship_data['type'] == '2') {
//                $pParam = $ship_data['type'] . ';'
//                . $ship_data['street'] . ';'
//                . $ship_data['number'] . ';'
//                . $ship_data['city'] . ';'
//                . $this->lionwheel_login['collect_city'] . ';'
//                . $this->lionwheel_login['collect_street'] . ';'
//                . $this->lionwheel_login['collect_street_number'] . ';'
//                . $this->lionwheel_login['collect_floor'] . ';'
//                . $this->lionwheel_login['collect_apartment'] . ';'
//                . $this->lionwheel_login['collect_phone'] . ';'
//                . $this->lionwheel_login['collect_email'] . ';'
//                . $this->lionwheel_login['collect_note'] . ';'
//                . $ship_data['contact_name'].' '.$ship_data['contact_phone'].' '.$ship_data['company']  . ';'
//                . $this->lionwheel_login['collect_company'] . ';'
//                . $ship_data['note'] . ';'
//                //. $ship_data['urgent'] . ';'
//                . $ship_data['lionwheel_static'] . ';'
//                //. $ship_data['motor'] . ';'
//                . $ship_data['packages'] . ';'
//                . $ship_data['return'] . ';'
//                . $ship_data['lionwheel_static'] . ';'
//                . $ship_data['woo_id'] . ';'
//                . $this->lionwheel_login['code'] . ';'
//                . $ship_data['lionwheel_static'] . ';'
//                . $ship_data['extra_note'] . ';'
//                . $ship_data['lionwheel_static'] . ';'
//                . $ship_data['lionwheel_empty'] . ';'
//                . $ship_data['lionwheel_empty'] . ';'
//                . $this->lionwheel_login['collect_company']  . ';'
//                . $ship_data['contact_phone'] . ';'
//                . $ship_data['contact_mail'] . ';'
//                . $ship_data['exaction_date'] . ';'
//                . $ship_data['collect'];
//
//            } else {
                $pParam = $ship_data['type'] . ';'
                . $this->lionwheel_login['collect_city'] . ';'
                . $this->lionwheel_login['collect_street'] . ';'
                . $this->lionwheel_login['collect_street_number'] . ';'
                . $this->lionwheel_login['collect_floor'] . ';'
                . $this->lionwheel_login['collect_apartment'] . ';'
                . $this->lionwheel_login['collect_phone'] . ';'
                . $this->lionwheel_login['collect_email'] . ';'
                . $this->lionwheel_login['collect_note'] . ';'
                . $ship_data['street'] . ';'
                . $ship_data['number'] . ';'
                . $ship_data['city'] . ';'
                . $this->lionwheel_login['collect_company'] . ';'
                . $ship_data['company'] . ';'
                . $ship_data['note'] . ';'
                //. $ship_data['urgent'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                //. $ship_data['motor'] . ';'
                . $ship_data['packages'] . ';'
                . $ship_data['return'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['woo_id'] . ';'
                . $this->lionwheel_login['code'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['extra_note'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['lionwheel_empty'] . ';'
                . $ship_data['lionwheel_empty'] . ';'
                . $ship_data['contact_name'] . ';'
                . $ship_data['contact_phone'] . ';'
                . $ship_data['contact_mail'] . ';'
                . $ship_data['exaction_date'] . ';'
                . $ship_data['collect'];


            //}
 if($ship_data['contact_name'] == ' ') {
     $contact_name = $ship_data['billing_contact_name'];
 } else {
     $contact_name =$ship_data['contact_name'];
 }
 if (empty($ship_data['number'])) {
     if (empty( $ship_data['billing_number'])) {
         $destination_number = '';
     } else {
         $destination_number = $ship_data['billing_number'];
     }
 } else {
     $destination_number = $ship_data['number'];
 }
            $exaction_date =  date("d/m/Y", strtotime($ship_data['exaction_date']));

            $data_send = array(
                'pickup_at' => $exaction_date,
                'original_order_id' => $ship_data['woo_key_id'],
                'destination_city' => $ship_data['city'] ? : $ship_data['billing_city'],
                'destination_street' => $ship_data['street'] ? : $ship_data['billing_street'],
                'destination_number' => $destination_number,
                'destination_recipient_name' => $contact_name,
                'destination_phone' => $ship_data['contact_phone'] ? : 'no phone',
            );
            if (!empty($ship_data['note'])) {
                $data_send['notes'] = $ship_data['note'];
            }
            if (!empty($this->lionwheel_login['collect_city'])) {
                $data_send['source_city'] = $this->lionwheel_login['collect_city'];
            }
            if (!empty($this->lionwheel_login['collect_street'])) {
                $data_send['source_street'] = $this->lionwheel_login['collect_street'];
            }
            if (!empty($this->lionwheel_login['collect_street_number'])) {
                $data_send['source_number'] = $this->lionwheel_login['collect_street_number'];
            }
            if (!empty($this->lionwheel_login['collect_floor'])) {
                $data_send['source_floor'] = $this->lionwheel_login['collect_floor'];
            }
            if (!empty($this->lionwheel_login['collect_apartment'])) {
                $data_send['source_apartment'] = $this->lionwheel_login['collect_apartment'];
            }
            if (!empty($this->lionwheel_login['collect_note'])) {
                $data_send['source_notes'] = $this->lionwheel_login['collect_note'];
            }
            if (!empty($this->lionwheel_login['lionwheel_username'])) {
                $data_send['source_recipient_name'] = $this->lionwheel_login['lionwheel_username'];
            }
            if (!empty($this->lionwheel_login['collect_phone'])) {
                $data_send['source_phone'] = $this->lionwheel_login['collect_phone'];
            }
            if (!empty($this->lionwheel_login['collect_email'])) {
                $data_send['source_email'] = $this->lionwheel_login['collect_email'];
            }
            if (!empty($ship_data['destination_floor'])) {
                $data_send['destination_floor'] = '';
            }
            if (!empty($ship_data['destination_apartment'])) {
                $data_send['destination_apartment'] = '';
            }
            if (!empty($ship_data['note'])) {
                $data_send['destination_notes'] = $ship_data['note'];
            }
            if (!empty($ship_data['contact_mail'])) {
                $data_send['destination_email'] = $ship_data['contact_mail'];
            }
            if (!empty($ship_data['delivery_method'])) {
                $data_send['delivery_method'] = '';
            }
            if (!empty($ship_data['return'])) {
                if($ship_data['return'] === "false") {
                    $data_send['is_roundtrip'] = false;
                    } elseif($ship_data['return'] === "true") {
                    $data_send['is_roundtrip'] = true;
                } else {
                    $data_send['is_roundtrip'] = $ship_data['return'];
                }
            }
            if (!empty($ship_data['packages'])) {
                $data_send['packages_quantity'] = (int) $ship_data['packages'];
            }
            if (!empty($ship_data['collect'])) {
                $data_send['money_collect'] = (int) $ship_data['collect'];
            }

            // Order Items
            $line_items = array();
            $order = wc_get_order( $ship_data['woo_id'] );
            foreach ($order->get_items() as $item_id => $item ) {
                $product_variation_id = $item['variation_id'];

                if ($product_variation_id) {
                    $product = new WC_Product($item['variation_id']);
                } else {
                    $product = new WC_Product($item['product_id']);
                }

                $item_data = array(
                    'name' => $item->get_name(),
                    'quantity' => $item->get_quantity(),
                    'sku' => $product->get_sku(),
                    'price' => $product->get_price(),
                );

                $line_items[] = $item_data;
            }
            if (!empty($line_items)) {
                $data_send['line_items'] = json_encode($line_items);
            }

            try {
	            error_log( sprintf( 'Lionwheel shipping data: %s', print_r( $data_send, true ) ) );


                // !?? TAPUZ_SAVE_NEW -IS UNDEFINED
                $lionwheel_response = $this->lionwheel_connection($data_send, TAPUZ_SAVE_NEW);


                return $this->lionwheel_response_json($lionwheel_response);
            } catch (Exception $e) {

                print_r($e->getMessage());
                die();
            }
        }

        public function change_ship_status($lionwheel_id)
        {
            $data = array();
            $data['p1'] = $this->lionwheel_login['code'];
            $data['p2'] = $lionwheel_id;
            $data['p3'] = '8';
            try {
                // !?? TAPUZ_CHANGE_STATUS is undefined
                $response = $this->lionwheel_connection($data, TAPUZ_CHANGE_STATUS);
                return $this->lionwheel_response_json($response);
            } catch (Exception $e) {
                print_r($e->getMessage());
                die();
            }
        }
    }
    
    
