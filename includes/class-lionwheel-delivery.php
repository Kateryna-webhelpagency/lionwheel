<?php

/**
 * Class Lionwheel_Delivery
 *
 * This is the plugin core class
 *
 */
class Lionwheel_Delivery {

	public $lionwheel_admin_view;

	public $lionwheel_ajax;

	public function __construct() {
		$this->load_dependencies();
		$this->lionwheel_admin_view = new Lionwheel_admin_view();
		$this->lionwheel_ajax = new Lionwheel_ajax();
	}
	/**
	 * Load dependencies
	 * @since 1.0.0
	 *
	 */
	private function load_dependencies() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-lionwheel-admin-view.php';
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/lionwheel-woocommerce-settings.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/lionwheel-woocommrerce-settings-subpage.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-lionwheel-ajax.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'labels/lionwheel-create-label.php';

	}

	/**
	 * Set the admin WordPress hooks
	 * @since 1.0.0
	 *
	 */
	private function set_admin_hooks() {
		add_action( 'admin_enqueue_scripts', array( $this->lionwheel_admin_view, 'enqueue_styles' ));
		add_action( 'admin_enqueue_scripts', array( $this->lionwheel_admin_view, 'enqueue_scripts' ));
		//add_action( 'admin_menu', 'register_lionwheel_menu' );
        add_action( 'admin_menu', 'register_lionwheel_settings_submenu' );
		add_action( 'add_meta_boxes', array( $this->lionwheel_admin_view, 'meta_boxes' ));
		add_action( 'wp_ajax_lionwheel_open_new_order', array( $this->lionwheel_ajax, 'lionwheel_open_new_order' ));
		add_action( 'wp_ajax_lionwheel_get_order_details', array( $this->lionwheel_ajax, 'lionwheel_get_order_details' ));
		add_action( 'wp_ajax_lionwheel_change_order_status', array( $this->lionwheel_ajax, 'lionwheel_change_order_status' ));
		add_action( 'wp_ajax_lionwheel_reopen_ship', array( $this->lionwheel_ajax, 'lionwheel_reopen_ship' ));
		add_action( 'admin_init', array($this,'lionwheel_plugins_loaded'), 20 );
		add_action( 'manage_shop_order_posts_custom_column' , array( $this->lionwheel_admin_view, 'lionwheel_admin_column' ), 3,2 );


	}

	/**
	 * Set the admin WordPress filters
	 * @since 1.1
	 *
	 */
	private function set_admin_filters() {
		add_filter( 'manage_shop_order_posts_columns',  array($this->lionwheel_admin_view,'lionwheel_admin_column_head'),50 );
	}

	/**
	 * Init method after class is created
	 * @since 1.0.0
	 *
	 */
	public function run() {
        $this->set_admin_hooks();
		$this->set_admin_filters();
	}

	/**
	 * Listen to GET request for labels
	 * @since 1.0.0
	 *
	 */
	public function lionwheel_plugins_loaded() {
		global $pagenow;

		if ($pagenow =='post.php' &&  isset($_GET['lionwheel_pdf']) && $_GET['lionwheel_pdf']=='create') {
			if (!wp_verify_nonce($_GET['lionwheel_label_wpnonce'], 'lionwheel_create_label' ) ) die( 'Failed security check' );

			$ship_id = isset($_GET['ship_id']) ? $_GET['ship_id'] : 0;
			$ship_data = get_post_meta( $_GET['order_id'], '_lionwheel_ship_data' );
			$ship_data[$ship_id]['collect_company'] = get_option('lionwheel_collect_company_name');
			$ship_data[$ship_id]['collect_street'] = get_option('lionwheel_collect_street_name');
			$ship_data[$ship_id]['collect_street_number'] = get_option('lionwheel_collect_street_number');
			$ship_data[$ship_id]['collect_city'] = get_option('lionwheel_collect_city_name');
			//  $ship_data[0]['ship_id'] = $ship_id;

			$pdf = new Lionwheel_create_label($ship_data , $ship_id, $_GET['order_id']);
			$lionwheel_paper_size = get_option('lionwheel_paper_size');
			if ($lionwheel_paper_size == '1904'){
				$pdf->create_dymo_label();
			} elseif ($lionwheel_paper_size == 'A4-logo' ){
				$lionwheel_logo_url_db = get_option( 'lionwheel_logo_url' );
				if (empty($lionwheel_logo_url_db)){
					$pdf->create_a4_label();
				} else {
					$pdf->create_a4_label_logo(get_option('lionwheel_logo_url'));
				}
			} elseif ($lionwheel_paper_size == 'A4'){
				$pdf->create_a4_label();
			}
			exit();
		}
	}
}
