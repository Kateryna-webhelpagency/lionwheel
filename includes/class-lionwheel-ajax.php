<?php

/**
 * Class lionwheel_ajax
 *
 * This is used to handle all ajax requests form admin meta box
 */
class Lionwheel_ajax {

	private $lionwheel_service;

	public function __construct() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-lionwheel-web-service.php';
		$this->lionwheel_service = new Lionwheel_service();
	}

	/**
	 * Open new Lionwheel delivery and add it to order DB
	 * @since 1.0.0
	 *
	 */
	public function lionwheel_open_new_order() {

		$retrieved_nonce = $_REQUEST['lionwheel_wpnonce'];
		if ( ! wp_verify_nonce( $retrieved_nonce, 'submit_lionwheel_open_ship' ) ) {
			die( 'Failed security check' );
		}


		global $woocommerce;

		$order_id = $_REQUEST['lionwheel_order_id'];
		$order         = wc_get_order( $order_id );
        $woo_order_key = $order->get_order_key();

		$lionwheel_collect = "";
		if ( $_REQUEST['lionwheel_collect'] != 'NO' ) {
			$lionwheel_collect = $_REQUEST['lionwheel_collect'];
		}

		$shipping_details = array();
		$billing_details = array();
		$shipping_details = $order->get_address('shipping');
		$billing_details = $order->get_address('billing');

		$customer_note = $order->customer_message;
		$ship_data = array();

		$ship_data['street'] = $shipping_details['address_1'];
		$ship_data['number'] = $shipping_details['address_2'];
		$ship_data['city'] = $shipping_details['city'];
		$ship_data['company'] = $shipping_details['company'];
		$ship_data['note'] = $customer_note;
		//$ship_data['urgent'] = '1';
		//$ship_data['type'] = $_REQUEST['lionwheel_delivey_type'];
		//$ship_data['motor'] = '1';
		$ship_data['packages'] = $_REQUEST['lionwheel_packages'];
		$ship_data['return'] = isset($_REQUEST['lionwheel_return']) ? $_REQUEST['lionwheel_return'] : false;
		$ship_data['woo_id'] = $order_id;
        $ship_data['woo_key_id']    = $woo_order_key;
		$ship_data['extra_note'] = "";
		$ship_data['contact_name'] = $shipping_details['first_name'] . ' ' . $shipping_details['last_name'];
		$ship_data['contact_phone'] = $billing_details['phone'];
		$ship_data['contact_mail'] = $billing_details['email'];
		$ship_data['exaction_date'] = $_REQUEST['lionwheel_exaction_date'];
		$ship_data['collect'] = $_REQUEST['lionwheel_collect'];
		$ship_data['delivery_time'] = date("d-m-Y g-i-s");
        $ship_data['billing_street'] = $billing_details['address_1'];
        $ship_data['billing_number'] = $billing_details['address_2'] ? : "-";
        $ship_data['billing_city'] = $billing_details['city'];
        $ship_data['billing_company'] = $billing_details['company'];
        $ship_data['billing_contact_name'] = $billing_details["first_name"] . ' ' . $billing_details["last_name"];
        $ship_data['delivery_time'] = date("d-m-Y g-i-s");


		$response = $this->lionwheel_service->create_ship( $ship_data );

		//echo '$response in ajax';
		//var_dump($response);
		error_log( sprintf('Lionwheel plugin response: %s', $response ) );

		if (array_key_exists('error', $response) || empty($response)) {

			print_r('In array_key_exists-error<br>');
		    echo $response['error'];
        } else {

        	print_r('after array_key_exists-error<br>');
            $ship_data['delivery_number'] = $response['task_id'];
            $ship_data['public_id'] = $response['task_id'];
            $ship_data['destination_region_str'] = $response['destination_region_str'];
            $ship_data['pdf_url'] = $response['label'];
            add_post_meta( $order_id, '_lionwheel_ship_data', $ship_data );

//				$ship_type = '';
//				if ( $_REQUEST['lionwheel_delivey_type'] == '1' ) {
//					if ( $_REQUEST['lionwheel_return'] == '2' ) {
//						$ship_type = __( 'Double delivery', 'woo-lionwheel-delivery' );
//					} else {
//						$ship_type = __( 'regular delivery', 'woo-lionwheel-delivery' );
//					}
//				} elseif ( $_REQUEST['lionwheel_delivey_type'] == '2' ) {
//					$ship_type = __( 'Collecting delivery', 'woo-lionwheel-delivery' );
//				}

            $order->add_order_note( __( 'Shipping successfully created, shipping number: ', 'woo-lionwheel-delivery' ) . $response['task_id']);

        }

		wp_send_json_success();
	}

	/**
	 * Get Lionwheel delivery status
	 * @since 1.0.0
	 *
	 */
	public function lionwheel_get_order_details() {
		$retrieved_nonce = $_REQUEST['lionwheel_get_wpnonce'];
		if ( ! wp_verify_nonce( $retrieved_nonce, 'submit_lionwheel_get_ship' ) ) {
			die( 'Failed security check' );
		}
		$order_id              = $_REQUEST['lionwheel_order_id'];
		$lionwheel_ship_status     = $this->lionwheel_service->get_ship_status( $order_id );
		$lionwheel_ship_status_arr = (array) $lionwheel_ship_status['ListDeliveryDetails'];

		// var_dump($lionwheel_ship_status_arr);

		echo json_encode( $lionwheel_ship_status_arr );
		die();
	}

	/**
	 * Change Lionwheel delivery status
	 * @since 1.0.0
	 */
	public function lionwheel_change_order_status() {
		$retrieved_nonce = $_REQUEST['lionwheel_change_wpnonce'];
		if ( ! wp_verify_nonce( $retrieved_nonce, 'submit_lionwheel_change_ship' ) ) {
			die( 'Failed security check' );
		}
		$ship_id           = $_REQUEST['lionwheel_ship_id'];
		$order_id          = $_REQUEST['order_id'];
		$lionwheel_ship_status = $this->lionwheel_service->change_ship_status( $ship_id );

		if ( $lionwheel_ship_status[0] == '-100' ) {
			echo '-100';
			die();
		} elseif ( $lionwheel_ship_status[0] == '-999' ) {
			echo '-999';
			die();
		} else {

			global $woocommerce;
			$order = wc_get_order( $order_id );
			$order->add_order_note( $ship_id . __( ' Order canceled' ) );
			add_post_meta( $order_id, '_order_canceled', $ship_id );

			print_r( $lionwheel_ship_status[0] );
			die();
		}
	}

	/**
	 * Reopen Ship
	 * @since 1.0.0
	 */
	public function lionwheel_reopen_ship() {
		$retrieved_nonce = $_REQUEST['lionwheel_reopen_wpnonce'];
		if ( ! wp_verify_nonce( $retrieved_nonce, 'lionwheel_reopen_ship' ) ) {
			die( 'Failed security check' );
		}
		delete_post_meta( $_REQUEST['lionwheel_woo_order_id'], '_lionwheel_ship_data' );
		die();
	}


}